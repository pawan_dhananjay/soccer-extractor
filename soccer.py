__author__ = 'Marco Mariani'
__author__ = 'Simone Papalini'
__author__ = 'Federico Fioravanti'
import os
from rdflib import URIRef, BNode, Literal, Namespace, Graph, term, XSD
from rdflib.namespace import RDF, FOAF
import urllib
import json
import re
import datetime
import time





import logging
time = time.time()
date = datetime.datetime.fromtimestamp(time).strftime('%Y_%m_%d')
logging.basicConfig(filename="soccer-extractor_"+date+".log", filemode='w', level=logging.WARNING, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
#logging.warning("This message should go to the log file")


num = re.compile('\d{1,1}')

ca = "di_calcio_"
nazu="Nazionale_Under-"
naz="Nazionale_"
f="femminile_"
m="maschile_"



def chiamata_json(stringa):
    chiamata = urllib.urlopen(stringa)
    leggere = chiamata.read()
    deserializzo = json.loads(leggere)
    return deserializzo

def chiamata_json_template_nazionali(valore_template, variabile):
    valore_template = valore_template.replace(" ","_")    #serve ad es. per un valore come Paesi Baschi
    valore_variabile = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:Template:Naz%252F"+valore_template+"?filter="+variabile+"&procs=-Extractors,Structure")
    return valore_variabile

def tipo_di_dato(dato):          #riesco a capire il tipo di dato che ho; quindi se e' l'anno, la squadra oppure presenze-gol
    try:
        if match_anni.match(dato[0]):
            return 1
        elif match_presenze.match(dato[0]) or match_pres_interrogativo.match(dato[0]) or match_gol_interrogativo.match(dato[0]):
            return 3
        elif dato[0] == u"\u2192":
            if dato[1]['name'] != "":
                return 2
        else:
            return 4
    except:
        try:
            if dato[0]['name'] == "Naz" or dato[0]['name'] == "NazU":   #serve per vedere se siamo nelle squadre Nazionali
                return 5
            elif dato[0]['name'] != "":
                return 2
            else:
                return 4
        except :
            return 7  #se metto un logging.warning qui mi stampa piu' di una volta la stessa cosa nel file di log perche' questa funzione viene richiamata due o tre volte con lo stesso paramentro

def crea_grafo(variabile, j, dato,calc,team):
    if variabile == 1:

       dato[0]=dato[0].replace(' ','')
       '''
                                    QUESTA ISTRUZIONE E' SPOSTATA PRIMA DI FARE LA CHIAMATA ALLA FUNZIONE PERCHE' NEL CASO
                                    L'ANNO NON VIENE RICONOSCIUTO NON CREEREBBE NEL GRAFO IL VALORE DELLA PROPRIETA'
                                    CAREERSTATION,; COME AD ES. ANDREA_PIRLO__1 all'interno della risorsa Andrea_Pirlo
       g.add((risorsa+calc,n.careerStation, risorsa+calc+"__"+str(j)))
       '''
       #print calc, " ",dato," ",j, " ",variabile," ", risorsa+calc+"__"+str(j), " ", type(calc),
       try:
           if dato[0][4] == "-" and num.match(dato[0][5]):
               anni_split = dato[0].split('-')
               #print anni_split
               g.add((risorsa+calc+"__"+str(j), n.startYear, Literal(anni_split[0],datatype=XSD.gYear)))
               g.add((risorsa+calc+"__"+str(j), n.endYear, Literal(anni_split[1],datatype=XSD.gYear)))
       except:
           try:
               if dato[0][4] == "-":
                   anni_split = dato[0].split('-')
                   g.add((risorsa+calc+"__"+str(j), n.startYear, Literal(anni_split[0],datatype=XSD.gYear)))
           except:
               try:
                   g.add((risorsa+calc+"__"+str(j), n.startYear, Literal(dato[0],datatype=XSD.gYear)))
                   g.add((risorsa+calc+"__"+str(j), n.endYear, Literal(dato[0],datatype=XSD.gYear)))
               except:
                   logging.warning(calc.encode('utf-8')+"                -> B) Problemi nel match degli ANNI (controllare dato in wikipedia):  "+str(dato))
    elif variabile==2:
        try:
            if dato[0] == u"\u2192":
                res_nome_squad = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:Template:"+dato[1]['name']+"?filter=voce&procs=-Extractors,Structure")
            else:
                res_nome_squad = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:Template:"+dato[0]['name']+"?filter=voce&procs=-Extractors,Structure")
            #print res_nome_squad
            nome_squadra = res_nome_squad['result'][0][0]
            uri_squadra = nome_squadra.replace(" ","_")
            if not(match_svg.match(uri_squadra)):
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+uri_squadra))
            else:
                logging.warning(calc.encode('utf-8')+"                   -> C) Problemi nel match del NOME DELLA SQUADRA (controllare template in wikipedia):  "+str(dato))
        except:
            try:
                if dato[0] == u"\u2192":
                    res_nome_squad2=chiamata_json("http://jsonpedia.org/annotate/resource/json/it:Template:"+dato[1]['name']+"?filter=@type:reference&procs=-Extractors,Structure")
                else:
                    res_nome_squad2=chiamata_json("http://jsonpedia.org/annotate/resource/json/it:Template:"+dato[0]['name']+"?filter=@type:reference&procs=-Extractors,Structure")
                nome_squadra2 = res_nome_squad2['result'][1]['label']
                uri_squadra2 = nome_squadra2.replace(" ","_")
                if not(match_svg.match(uri_squadra2)):
                    g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+uri_squadra2))
                else:
                    logging.warning(calc.encode('utf-8')+"                   -> C) Problemi nel match del NOME DELLA SQUADRA (controllare template in wikipedia):  "+str(dato))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> C) Problemi nel match del NOME DELLA SQUADRA (controllare dato in wikipedia o problema dovuto a jsonpedia):  "+str(dato))
    elif variabile==3:
        dato[0]=dato[0].replace(' ','')
        i=0
        try:
            while (dato[0][i] != "(") and i!=len(dato[0])-1:
                i = i+1
            if i!=len(dato[0])-1:
                presenze_gol = dato[0].split(dato[0][i])
                numero_presenze = presenze_gol[0]
                numero_gol_parentesi = presenze_gol[1]

                numero_gol = numero_gol_parentesi.replace(' ','')
                numero_gol = numero_gol.replace('(','')
                numero_gol = numero_gol.replace(')','')
                #print numero_gol
                if match_interrogativo.match(numero_gol):
                    logging.warning(calc.encode('utf-8')+"                   -> D) non ci sono il numero di gol:  "+str(numero_gol))
                else:
                    g.add((risorsa+calc+"__"+str(j), n.numberOfGoals, Literal(numero_gol,datatype=XSD.integer)))
            else:
                numero_presenze=dato[0]
                '''
                TODO: controllo sul numero delle presenze per stabilire se i dati non siano corrotti
                '''
            #print numero_presenze
            if match_interrogativo.match(numero_presenze):
                    logging.warning(calc.encode('utf-8')+"                   -> D) non ci sono il numero di presenze:  "+str(numero_presenze))
            else:
                if int(numero_presenze)>800:
                    logging.warning(calc.encode('utf-8')+"                   -> D) numero di presenze strane da controllare:  "+str(numero_presenze))
                g.add((risorsa+calc+"__"+str(j), n.numberOfMatches, Literal(numero_presenze,datatype=XSD.integer)))

        except:
            logging.warning(calc.encode('utf-8')+"                   -> E) Problemi nel match delle PRESENZE-GOAL (controllare dato in wikipedia):  "+str(dato))

    elif variabile==4:
        logging.warning(calc.encode('utf-8')+"                   -> F) valore non che non fa il match con nessun formato:    "+str(dato))

    elif variabile==5:
        contenuto_template=dato[0]['content']
        stringa = ""
        if dato[0]['name']=="NazU" and len(contenuto_template)==4:
            stringa = nazu
            if contenuto_template['@an0'][0] != "CA":
                logging.warning(calc.encode('utf-8')+"                   -> G) Non stiamo parlando di calcio:  "+str(contenuto_template['@an0'][0]))
                return
            stringa = stringa + contenuto_template['@an3'][0] + "_" + ca
            if contenuto_template['@an2'][0] == "F":
                stringa = stringa + f
            elif contenuto_template['@an2'][0] == "M":
                stringa = stringa + m
            try:
                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"tipo")    #la ricerca di questo parametro serve ad es. nel caso delle selezioni
                if len(risultato['result']) != 0:
                    risultato = risultato['result'][0][0][0].upper()+risultato['result'][0][0][1:]  #la prima lettera diventa maiuscola(nel caso non lo fosse gia'); perche' deve poi essere sostituito a Nazionale e la prima lettera deve essere maiuscola
                    stringa = stringa.replace('Nazionale',risultato)

                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"d")
                if len(risultato['result']) != 0:
                    stringa = stringa + risultato['result'][0][0]
                else:
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"b")
                    stringa = stringa + risultato['result'][0][0]
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"c")
                    stringa = stringa + risultato['result'][0][0]
                stringa=stringa.replace('&#32;','_')
                stringa=stringa.replace(' ','_')
                #logging.warning(stringa)
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+stringa))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> H) problemi con uno, o piu', dei parametri b,c,d,tipo; vedere template: NAZ/"+str(contenuto_template['@an1'][0]))

        elif dato[0]['name']=="NazU" and len(contenuto_template)==3:
            stringa = nazu
            if contenuto_template['@an0'][0] != "CA":
                logging.warning(calc.encode('utf-8')+"                   -> G) Non stiamo parlando di calcio:  "+str(contenuto_template['@an0'][0]))
                return
            stringa = stringa + contenuto_template['@an2'][0]+ "_" + ca
            try:
                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"tipo")
                if len(risultato['result']) != 0:
                    risultato = risultato['result'][0][0][0].upper()+risultato['result'][0][0][1:]
                    stringa = stringa.replace('Nazionale',risultato)

                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"d")
                if len(risultato['result']) != 0:
                    stringa = stringa + risultato['result'][0][0]
                else:
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"b")
                    stringa = stringa + risultato['result'][0][0]
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"c")
                    stringa = stringa + risultato['result'][0][0]
                stringa=stringa.replace('&#32;','_')
                stringa=stringa.replace(' ','_')
                #logging.warning(stringa)
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+stringa))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> H) problemi con uno, o piu', dei parametri b,c,d,tipo; vedere template: NAZ/"+str(contenuto_template['@an1'][0]))

        elif dato[0]['name']=="Naz" and len(contenuto_template)==4:
            stringa = naz
            if contenuto_template['@an0'][0] != "CA":
                logging.warning(calc.encode('utf-8')+"                   -> G) Non stiamo parlando di calcio:  "+str(contenuto_template['@an0'][0]))
                return
            stringa = stringa + contenuto_template['@an3'][0]+ "_" + ca
            if contenuto_template['@an2'][0] == "F":
                stringa = stringa + f
            elif contenuto_template['@an2'][0] == "M":
                stringa = stringa + m
            try:
                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"tipo")
                if len(risultato['result']) != 0:
                    risultato = risultato['result'][0][0][0].upper()+risultato['result'][0][0][1:]
                    stringa = stringa.replace('Nazionale',risultato)

                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"d")
                if len(risultato['result']) != 0:
                    stringa = stringa + risultato['result'][0][0]
                else:
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"b")
                    stringa = stringa + risultato['result'][0][0]
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"c")
                    stringa = stringa + risultato['result'][0][0]
                stringa=stringa.replace('&#32;','_')
                stringa=stringa.replace(' ','_')
                #logging.warning(stringa)
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+stringa))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> H) problemi con uno, o piu', dei parametri b,c,d,tipo; vedere template: NAZ/"+str(contenuto_template['@an1'][0]))

        elif dato[0]['name']=="Naz" and len(contenuto_template)==3:
            stringa = naz
            if contenuto_template['@an0'][0] != "CA":
                logging.warning(calc.encode('utf-8')+"                   -> G) Non stiamo parlando di calcio:  "+str(contenuto_template['@an0'][0]))
                return
            #stringa = stringa + contenuto_template['@an3'][0] + ca
            if contenuto_template['@an2'][0] == "F":
                stringa = stringa + ca + f
            elif contenuto_template['@an2'][0] == "M":
                stringa = stringa + ca + m
            elif contenuto_template['@an2'][0] !="":
                stringa = stringa + contenuto_template['@an2'][0]+ "_" + ca
            try:
                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"tipo")
                if len(risultato['result']) != 0:
                    risultato = risultato['result'][0][0][0].upper()+risultato['result'][0][0][1:]
                    stringa = stringa.replace('Nazionale',risultato)

                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"d")
                if len(risultato['result']) != 0:
                    stringa = stringa + risultato['result'][0][0]
                else:
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"b")
                    stringa = stringa + risultato['result'][0][0]
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"c")
                    stringa = stringa + risultato['result'][0][0]
                stringa=stringa.replace('&#32;','_')
                stringa=stringa.replace(' ','_')
                #logging.warning(stringa)
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+stringa))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> H) problemi con uno, o piu', dei parametri b,c,d,tipo; vedere template: NAZ/"+str(contenuto_template['@an1'][0]))

        elif dato[0]['name']=="Naz" and len(contenuto_template)==2:
            stringa = naz
            if contenuto_template['@an0'][0] != "CA":
                logging.warning(calc.encode('utf-8')+"                   -> G) Non stiamo parlando di calcio:  "+str(contenuto_template['@an0'][0]))
                return
            stringa = stringa + ca
            try:
                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"tipo")
                if len(risultato['result']) != 0:
                    risultato = risultato['result'][0][0][0].upper()+risultato['result'][0][0][1:]
                    stringa = stringa.replace('Nazionale',risultato)

                risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"d")
                #qui come negli altri casi va gestito il try exception nel caso ci siano nei template dati non validi
                if len(risultato['result']) != 0:
                    stringa = stringa + risultato['result'][0][0]
                else:
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"b")
                    stringa = stringa + risultato['result'][0][0]
                    risultato = chiamata_json_template_nazionali(contenuto_template['@an1'][0],"c")
                    stringa = stringa + risultato['result'][0][0]
                stringa=stringa.replace('&#32;','_')
                stringa=stringa.replace(' ','_')
                #logging.warning(stringa)
                g.add((risorsa+calc+"__"+str(j), URIRef(n+team), risorsa+stringa))
            except:
                logging.warning(calc.encode('utf-8')+"                   -> H) problemi con uno, o piu', dei parametri b,c,d,tipo; vedere template: NAZ/"+str(contenuto_template['@an1'][0]))

        else:
            logging.warning(calc.encode('utf-8')+"                   -> I) Formattazione del template Naz o NazU non corretta o particolare")

    elif variabile == 7:
        pass


    else:
        logging.warning(calc.encode('utf-8')+"                   -> L) qualcosa non e' andato bene; e' stata ritornata una variabile non riconosciuta")








"""
match_anni = re.compile('\d{4,4}-\d{4,4}$')
match_anno = re.compile('\d{4,4}$')
match_cont = re.compile('\d{4,4}-$')
"""
match_anni = re.compile('\d{4,4}\s*(-\s*(\d{4,4}\s*)?)?$')  #racchiude gli altri tre in uno

"""
match_presenze = re.compile('\d+\s\(\d*\)$')
match_pres_portieri = re.compile('\d+\s\(-\d+\)$')
"""
match_presenze = re.compile('\d+\s{0,}(\(-{0,1}\d*\))?$')

match_pres_interrogativo = re.compile('\?+\s{0,}(\(-{0,1}\d*\))?$')

match_gol_interrogativo = re.compile('\d+\s{0,}(\(-{0,1}\?*\))?$')

match_interrogativo = re.compile('\?+$')

match_svg = re.compile('.*\.svg.*')

RDF.type
# = rdflib.term.URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')

FOAF.knows
# = rdflib.term.URIRef(u'http://xmlns.com/foaf/0.1/knows')

g = Graph()

n = Namespace("http://dbpedia.org/ontology/")
risorsa = URIRef("http://it.dbpedia.org/resource/")



#questa serve per vedere il numero totale di calciatori cosi da poter iterare la query che prendono i calciatori, siccome hanno il limite dei mille elementi
totale_calciatori = chiamata_json("http://it.dbpedia.org/sparql?default-graph-uri=&query=select+count%28%3Fs%29+as+%3Fnumero_calciatori%0D%0Awhere%7B%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FSoccerPlayer%3E.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2FwikiPageUsesTemplate%3E+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fresource%2FTemplate%3ASportivo%3E%0D%0A%7D%0D%0A&format=application%2Fsparql-results%2Bjson&debug=on")
totale_calciatori = totale_calciatori['results']['bindings'][0]['numero_calciatori']['value']

logging.warning("NUMERO TOTALE DI CALCIATORI PRESENTI IN DBPEDIA.IT: "+totale_calciatori)

#contatore per log
count = 0

#limite per query
limit = 1000

#istanza di partenza
min = 0
logging.warning("ISTANZA DI PARTENZA: "+str(min))
#offset per query
offset = min

#istanza di arrivo
max = int(totale_calciatori)
#max = 10
logging.warning("ISTANZA DI ARRIVO: "+str(max))

logging.warning("NUMERO TOTALE DI CALCIATORI DA ELABORARE: "+str(max-min))


while(count < max-min):
#while(offset < totale_calciatori):
    #query = chiamata_json("http://it.dbpedia.org/sparql?default-graph-uri=&query=select+%3Fs+as+%3Fcalciatori%0D%0Awhere%7B%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FAthlete%3E.%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FSoccerPlayer%3E.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2Fdisciplina%3E+%22Calcio%22%40it.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2FwikiPageUsesTemplate%3E+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fresource%2FTemplate%3ASportivo%3E%0D%0A%7D+LIMIT+5%0D%0A&format=application%2Fsparql-results%2Bjson&debug=on")
    #query = chiamata_json("http://it.dbpedia.org/sparql?default-graph-uri=&query=select+%3Fs+as+%3Fcalciatori%0D%0Awhere%7B%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FAthlete%3E.%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FSoccerPlayer%3E.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2Fdisciplina%3E+%22Calcio%22%40it.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2FwikiPageUsesTemplate%3E+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fresource%2FTemplate%3ASportivo%3E%0D%0A%7D%0D%0AORDER+BY+%3Fcalciatori%0D%0ALIMIT+"+str(limit)+"%0D%0AOFFSET+"+str(offset)+"%0D%0A&format=application%2Fsparql-results%2Bjson&debug=on")
    #query = chiamata_json("http://it.dbpedia.org/sparql?default-graph-uri=&query=select+%3Fs+as+%3Fcalciatori%0D%0Awhere%7B%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FAthlete%3E.%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FSoccerPlayer%3E.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2Fdisciplina%3E+%22Calcio%22%40it.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2FwikiPageUsesTemplate%3E+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fresource%2FTemplate%3ASportivo%3E%0D%0A%7D+LIMIT+5%0D%0AOFFSET+"+str(offset)+"%0D%0A&format=application%2Fsparql-results%2Bjson&debug=on")
    try:
        query = chiamata_json("http://it.dbpedia.org/sparql?default-graph-uri=&query=select+%3Fs+as+%3Fcalciatori%0D%0Awhere%7B%0D%0A%3Fs+a+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2FSoccerPlayer%3E.%0D%0A%3Fs+%3Chttp%3A%2F%2Fit.dbpedia.org%2Fproperty%2FwikiPageUsesTemplate%3E%0D%0A%3Chttp%3A%2F%2Fit.dbpedia.org%2Fresource%2FTemplate%3ASportivo%3E%0D%0A%7D%0D%0AOFFSET+"+str(offset)+"%0D%0ALIMIT+"+str(limit)+"&format=application%2Fsparql-results%2Bjson&debug=on")
    except:
        query=False
        explose=offset+limit
        logging.exception("ESTRATTORE IN ECCEZIONE: PERSI CALCIATORI DAL  "+str(offset)+" AL "+str(explose)+", SEGUE REPORT: ")
	count += limit

    offset = offset + limit
    if (query!=False):
        risultato_query = query['results']['bindings']
        for calciatore in risultato_query:    #con questo ciclo riesco a lavorare con un singolo calciatore
            if (count < max-min):
		count += 1
		logging.warning("NUMERO ISTANZA ANALIZZATA: "+str(count))
                try:
                    n_career_station=0
                    variabile_precedente = 0     #variabile per vedere il tipo di dato ritornato nella precedente chiamata alla funzione tipo di dato
                    variabile_successiva = 0
                    url_calciatore = calciatore['calciatori']['value']    #http://it.dbpedia.org/resource/calciatore
                    #print calciatore['calciatori']['value']

                    nome_calciatore = url_calciatore.replace("http://it.dbpedia.org/resource/","")    #ottengo il nome_cognome del calciatore

                    #encoded_nome_calciatore = nome_calciatore.encode('utf-8')
                    #logging.warning(str(k)+ ")  " + encoded_nome_calciatore+" :")



                    nome_calciatore_encode = nome_calciatore.encode('utf-8')    #questo serve per quei calciatori che hanno particolari accenti/lettere nel nome


                    #print nome_calciatore_encode
                    squadre_giovanili = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:"+nome_calciatore_encode+"?filter=SquadreGiovanili>content&procs=-Extractors,Structure")
                    result_giovanili = squadre_giovanili['result']

                    if len(result_giovanili) != 0:    #se la lunghezza e' nulla vuol dire che il calciatore non ha il template carriera sportivo riferito alle giovanili
                        for giovanili in result_giovanili:   #cosi scorro, se ci sono, i diversi template carriera sportivo delle giovanili
                            num_elementi = len(giovanili)    #vedo il numero di elementi nel singolo template carriera sportivo
                            for i in range(num_elementi):
                                variabile = tipo_di_dato(giovanili["@an"+str(i)])
                                if variabile == 7:
                                    logging.warning(nome_calciatore_encode+"                   -> A) Il dato del template e' errato: "+str(giovanili["@an"+str(i)]))

                                if i != 0:                                                          #questi controlli sono per vedere i tipi di dato precedenti e successivi a quello attuale; cosi' da risolvere il problema nel caso gli anni di una calciatore siano in formato sbagliato
                                    variabile_precedente = tipo_di_dato(giovanili["@an"+str(i-1)])
                                if i < num_elementi-1:
                                    variabile_successiva = tipo_di_dato(giovanili["@an"+str(i+1)])

                                if variabile == 1 or (variabile==4 and variabile_precedente==3) or (variabile==4 and variabile_successiva==2) or (variabile==4 and variabile_successiva==5):
                                    n_career_station=n_career_station+1
                                    g.add((risorsa+nome_calciatore,n.careerStation, risorsa+nome_calciatore+"__"+str(n_career_station)))
                                crea_grafo(variabile, n_career_station, giovanili["@an"+str(i)],nome_calciatore,"youthClub")

                                if variabile==4 and variabile_precedente==4 and variabile_successiva==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono TRE tipi di dato NON RICONOSCIUTI consecutivi")
                                elif variabile==4 and variabile_precedente==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono DUE tipi di dato NON RICONOSCIUTI consecutivi")


                    squadre_principali = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:"+nome_calciatore_encode+"?filter=Squadre>content&procs=-Extractors,Structure")
                    result_squadre = squadre_principali['result']

                    if len(result_squadre) != 0:    #se la lunghezza e' nulla vuol dire che il calciatore non ha il template carriera sportivo riferito alle squadre
                        for squadre in result_squadre:   #cosi scorro, se ci sono, i diversi template carriera sportivo delle squadre principali
                            num_elementi = len(squadre)    #vedo il numero di elementi nel singolo template carriera sportivo
                            for i in range(num_elementi):
                                variabile = tipo_di_dato(squadre["@an"+str(i)])
                                if variabile == 7:
                                    logging.warning(nome_calciatore_encode+"                   -> A) Il dato del template e' errato: "+str(squadre["@an"+str(i)]))

                                if i != 0:
                                    variabile_precedente = tipo_di_dato(squadre["@an"+str(i-1)])
                                if i < num_elementi-1:
                                    variabile_successiva = tipo_di_dato(squadre["@an"+str(i+1)])
                                if variabile == 1 or (variabile==4 and variabile_precedente==3) or (variabile==4 and variabile_successiva==2) or (variabile==4 and variabile_successiva==5):
                                    n_career_station=n_career_station+1
                                    g.add((risorsa+nome_calciatore, n.careerStation, risorsa+nome_calciatore+"__"+str(n_career_station)))
                                crea_grafo(variabile, n_career_station, squadre["@an"+str(i)],nome_calciatore,"team")

                                if variabile==4 and variabile_precedente==4 and variabile_successiva==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono TRE tipi di dato NON RICONOSCIUTI consecutivi")
                                elif variabile==4 and variabile_precedente==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono DUE tipi di dato NON RICONOSCIUTI consecutivi")

                    squadre_nazionali = chiamata_json("http://jsonpedia.org/annotate/resource/json/it:"+nome_calciatore_encode+"?filter=SquadreNazionali>content&procs=-Extractors,Structure")
                    result_nazionali = squadre_nazionali['result']

                    if len(result_nazionali) != 0:    #se la lunghezza e' nulla vuol dire che il calciatore non ha il template carriera sportivo riferito alle nazionali
                        for nazionali in result_nazionali:   #cosi scorro, se ci sono, i diversi template carriera sportivo delle squadre nazionali
                            num_elementi = len(nazionali)    #vedo il numero di elementi nel singolo template carriera sportivo
                            for i in range(num_elementi):
                                variabile = tipo_di_dato(nazionali["@an"+str(i)])
                                if variabile == 7:
                                    logging.warning(nome_calciatore_encode+"                   -> A) Il dato del template e' errato: "+str(nazionali["@an"+str(i)]))

                                if i != 0:
                                    variabile_precedente = tipo_di_dato(nazionali["@an"+str(i-1)])
                                if i < num_elementi-1:
                                    variabile_successiva = tipo_di_dato(nazionali["@an"+str(i+1)])
                                if variabile == 1 or (variabile==4 and variabile_precedente==3) or (variabile==4 and variabile_successiva==2) or (variabile==4 and variabile_successiva==5):
                                    n_career_station=n_career_station+1
                                    g.add((risorsa+nome_calciatore,n.careerStation, risorsa+nome_calciatore+"__"+str(n_career_station)))
                                crea_grafo(variabile, n_career_station, nazionali["@an"+str(i)],nome_calciatore,"nationalTeam")

                                if variabile==4 and variabile_precedente==4 and variabile_successiva==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono TRE tipi di dato NON RICONOSCIUTI consecutivi")
                                elif variabile==4 and variabile_precedente==4:
                                    logging.warning(nome_calciatore_encode+"                   -> M) ci sono DUE tipi di dato NON RICONOSCIUTI consecutivi")
                except:
                    logging.exception("ESTRATTORE IN ECCEZIONE, PERSO CALCIATORE "+ nome_calciatore_encode +", SEGUE REPORT: ")

g.serialize("soccer_"+date+".ttl", format="turtle")
